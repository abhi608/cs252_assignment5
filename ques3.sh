#!/bin/bash

while IFS='' read -r line || [[ -n "$line" ]]; do
    echo "xyz" | openssl s_client -connect $line | openssl x509 -pubkey -noout >> $2
done < "$1"
